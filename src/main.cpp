#include <iostream>
#include "../include/Record.h"
#include "../include/FileInterface.h"
#include <vector>

int main(int argc, int *argv[])
{
	FILE *file;

	// I had to use this path because I was using Visual Studio, however,
	// you'll likely need to simply use "data/data.txt" if you're doing
	// this in a Linux/Unix environment.
	char path[] = { "../fileinterface/data/data.txt" };

	FileInterface file_interface(path); // Opens file.
	Record record;
	std::vector<Record> v_record;

	// Read all the contents of the file.
	while (file_interface.Read(record))
	{
		v_record.push_back(record);
	}
	
	// Print out the file contents.
	for (int i = 0; i < static_cast<signed>(v_record.size()); ++i)
	{
		std::cout << v_record[i] << std::endl;
	}

	return 0;
}
