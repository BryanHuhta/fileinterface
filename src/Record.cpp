#include "../include/Record.h"

Record::Record()
{
	integer_field = NULL;
	string_field = "";
}

Record::Record(const int& _integer_field, const std::string& _string_field)
{
	integer_field = _integer_field;
	string_field = _string_field;
}

int Record::get_integer_field() const
{
	return integer_field;
}

std::string Record::get_string_field() const
{
	return string_field;
}

void Record::set_integer_field(const int& _integer_field)
{
	integer_field = _integer_field;
}

void Record::set_string_field(const std::string& _string_field)
{
	string_field = _string_field;
}
