#include "../include/FileInterface.h"

FileInterface::FileInterface()
{
	file = nullptr;
}

FileInterface::FileInterface(const char* _path)
{
	// Note: fopen may return and error code, it should be handled.
	file = fopen(_path, "r");
}

FileInterface::~FileInterface()
{
	// Note: Technically, fclose returns an error code, that probably
	// should be handled as well.
	fclose(file);
}

// Write can be implemented in a similar fashion, with the exception
// being using fwrite() instead of fread(). Additionally, when opening the
// file (fopen()), the permissions need to be set to "r+" or "w+" instead
// of "r".
bool FileInterface::Read(Record& _record) const
{
	// Record size is 7 bytes:
	//     - integer is 2 bytes
	//     - string is 5 bytes (5 characters)

	char integer_buffer[2];
	char string_buffer[6];
	int bytes_read = 0;
	
	// Save read into buffer, read 1 byte at a time, read 2 time,
	// from the file that 'file' is pointing to.
	// fread will return the number of bytes read.
	bytes_read = fread(integer_buffer, 1, 2, file);
	if (bytes_read != 2)
	{
		return false;
	}
	_record.set_integer_field(atoi(integer_buffer));

	bytes_read = fread(string_buffer, 1, 5, file);
	if (bytes_read != 5)
	{
		return false;
	}
	// Null terminate the string.
	string_buffer[5] = '\0';
	_record.set_string_field(string_buffer);

	return true;
}

