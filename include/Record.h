#pragma once

#include <string>
#include <ostream>

class Record
{
private:
	int integer_field;
	std::string string_field;
public:
	Record();
	Record(const int&, const std::string&);

	int get_integer_field() const;
	std::string get_string_field() const;
	void set_integer_field(const int& integer_field);
	void set_string_field(const std::string& string_field);

	friend std::ostream& operator<<(std::ostream& _ostream, const Record& _record)
	{
		return _ostream
			<< "integer: " << _record.integer_field
			<< "\tstring: " << _record.string_field;
	}
};

