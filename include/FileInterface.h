#pragma once

#include <fstream>

#include "Record.h"

class FileInterface
{
private:
	FILE *file;

public:
	FileInterface();
	explicit FileInterface(const char*);
	~FileInterface();

	bool Read(Record&) const;
};

